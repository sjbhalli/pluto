package com.pluto.pluto;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import com.amazonaws.auth.CognitoCachingCredentialsProvider;
import com.amazonaws.regions.Regions;
import com.reimaginebanking.api.nessieandroidsdk.NessieError;
import com.reimaginebanking.api.nessieandroidsdk.NessieResultsListener;
import com.reimaginebanking.api.nessieandroidsdk.requestclients.NessieClient;

import java.util.Random;

public class LoginActivity extends Activity {

    EditText usernameET;
    EditText passwordET;

    String username;
    String password;
    User user;

    DynamoDBTool dbTool;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        Log.i("MainActivity", "Setting content view.");
        setContentView(R.layout.login);

        CognitoCachingCredentialsProvider credentialsProvider = new CognitoCachingCredentialsProvider(
                getApplicationContext(),
                "us-west-2:7267bf4e-7197-42f4-b411-a71af21233a6", // Identity Pool ID
                Regions.US_WEST_2 // Region
        );

        dbTool = new DynamoDBTool(credentialsProvider);

        setBackgroundImage();

        usernameET = (EditText) findViewById(R.id.username_edittext);
        passwordET = (EditText) findViewById(R.id.password_edittext);

        findViewById(R.id.login_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                username = usernameET.getText().toString();
                password = passwordET.getText().toString();

                if (isValidUserInfo()) {
                    user = dbTool.getUser(username);
                    startMainActivity();
                }
            }
        });
    }

    private void setBackgroundImage() {
        int randomBackgroundImage = 2;//(new Random()).nextInt(5);
        System.out.println(randomBackgroundImage);
        if (randomBackgroundImage == 0) {
//            ((ImageView)findViewById(R.id.background_img)).setImageResource(R.drawable.nyc_skyscrapers);
        } else if (randomBackgroundImage == 1) {
//            ((ImageView)findViewById(R.id.background_img)).setImageResource(R.drawable.city_crosswalk);
        } else if (randomBackgroundImage == 2) {
            ((ImageView)findViewById(R.id.background_img)).setImageResource(R.drawable.rocky_mountains);
        } else if (randomBackgroundImage == 3) {
//            ((ImageView)findViewById(R.id.background_img)).setImageResource(R.drawable.earth);
        } else if (randomBackgroundImage == 5) {
//            ((ImageView)findViewById(R.id.background_img)).setImageResource(R.drawable.waterfall);
        } else {
            ((ImageView)findViewById(R.id.background_img)).setImageResource(R.drawable.rocky_mountains);
        }
    }

    private boolean isValidUserInfo() {
        System.out.println("username: " + username);
        System.out.println("password: " + password);

        return dbTool.logInUser(username, password);
    }

    private void startMainActivity() {
        Intent mainActivityIntent = new Intent(this, MainActivity.class);

        mainActivityIntent.putExtra("username", username);
        mainActivityIntent.putExtra("password", password);
        mainActivityIntent.putExtra("firstName", user.getFullName().split(" ")[0]);
        mainActivityIntent.putExtra("lastName", user.getFullName().split(" ")[1]);

        this.startActivity(mainActivityIntent);

        overridePendingTransition(R.anim.activity_enter_animation, R.anim.activity_exit_animation);
    }

}
