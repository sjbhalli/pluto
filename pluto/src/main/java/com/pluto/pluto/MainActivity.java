package com.pluto.pluto;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.amazonaws.auth.CognitoCachingCredentialsProvider;
import com.amazonaws.regions.Regions;
import com.reimaginebanking.api.nessieandroidsdk.NessieError;
import com.reimaginebanking.api.nessieandroidsdk.NessieResultsListener;
import com.reimaginebanking.api.nessieandroidsdk.requestclients.NessieClient;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.regex.Pattern;

public class MainActivity extends Activity {

//    NessieClient client = NessieClient.getInstance("9a233163a933edb4616ff218a63db9d8");
    NessieClient client = NessieClient.getInstance("0cf6059f76c6ff57bdc388a89c118a66");

    public static NumberFormat currencyFormatter = NumberFormat.getCurrencyInstance(new Locale("en", "US"));

    String username = "";
    String password = "";
    String firstName = "";
    String lastName = "";

    public static DynamoDBTool dbTool;
    User user;

    Account latestCheckingsAccount;
    Account latestSavingsAccount;

    ArrayList<Box> boxes = new ArrayList<Box>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        Log.i("MainActivity", "Setting content view.");
        setContentView(R.layout.main);

        Log.i("MainActivity", "Getting extras from calling intent.");
        Intent callingIntent = getIntent();
        username = callingIntent.getStringExtra("username");
        password = callingIntent.getStringExtra("password");
        firstName = callingIntent.getStringExtra("firstName");
        lastName = callingIntent.getStringExtra("lastName");

        CognitoCachingCredentialsProvider credentialsProvider = new CognitoCachingCredentialsProvider(
                getApplicationContext(),
                "us-west-2:7267bf4e-7197-42f4-b411-a71af21233a6", // Identity Pool ID
                Regions.US_WEST_2 // Region
        );

        dbTool = new DynamoDBTool(credentialsProvider);
        user = dbTool.getUser(username);

        setGreetingAndDate();
        populateWithAccounts();
    }

    @Override
    protected void onResume() {
        System.out.println("RESUME");
        user = dbTool.getUser(username);
        populateWithAccounts();

        super.onResume();
    }

    private void setGreetingAndDate() {
        Log.i("MainActivity", "Getting date and time info.");
        Calendar calendar = GregorianCalendar.getInstance();
        int hourOfDay = calendar.get(Calendar.HOUR_OF_DAY);

        if (hourOfDay >= 0 && hourOfDay < 12) {
            ((TextView) findViewById(R.id.greeting_textview)).setText("Good Morning " + firstName + "!");
        } else if (hourOfDay >= 12 && hourOfDay < 17) {
            ((TextView) findViewById(R.id.greeting_textview)).setText("Good Afternoon " + firstName + "!");
        } else if (hourOfDay >= 17 && hourOfDay < 24) {
            ((TextView) findViewById(R.id.greeting_textview)).setText("Good Evening " + firstName + "!");
        } else {
            ((TextView) findViewById(R.id.greeting_textview)).setText("Hello " + firstName + "!");
        }

        ((TextView) findViewById(R.id.date_textview)).setText(getMonth(calendar.get(Calendar.MONTH)) + " " +
                calendar.get(Calendar.DATE) + ", " +
                calendar.get(Calendar.YEAR));
    }

    private String getMonth(int calendarReference) {
        switch (calendarReference) {
            case 0:
                return "January";
            case 1:
                return "February";
            case 2:
                return "March";
            case 3:
                return "April";
            case 4:
                return "May";
            case 5:
                return "June";
            case 6:
                return "July";
            case 7:
                return "August";
            case 8:
                return "September";
            case 9:
                return "October";
            case 10:
                return "November";
            case 11:
                return "December";
        }

        return "What's the month again?";
    }

    private void populateWithAccounts() {
        Bank bank = new Bank();
        System.out.println("user id: " + user.getUserID());
        bank.updateAccountsByCustomerId(user.getUserID());
    }

    private void placeViews() {
        final Context context = this;

        user = dbTool.getUser(username);
        LinearLayout cardsContainer = (LinearLayout) findViewById(R.id.accounts_cards_container);
        cardsContainer.removeAllViews();

        View checkingsAccountCard = getLayoutInflater().inflate(R.layout.checkings_account_card, null);
//        String lastFourDigitsOfCheckingsAccountId = latestCheckingsAccount.getAccountId().substring(latestCheckingsAccount.getAccountId().length() - 4);
//        ((TextView) checkingsAccountCard.findViewById(R.id.checkings_account_id_textview)).setText(lastFourDigitsOfCheckingsAccountId);
        ((TextView) checkingsAccountCard.findViewById(R.id.checkings_account_balance_textview))
                .setText(currencyFormatter.format(user.getCheckingsBalance()));

        checkingsAccountCard.findViewById(R.id.transfer_checking_to_savings).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent transferIntent = new Intent(context, TransferCheckingsToSavingsActivity.class);
                transferIntent.putExtra("username", user.getUsername());
                transferIntent.putExtra("checkingsBalance", user.getCheckingsBalance() + "");
                startActivity(transferIntent);
            }
        });

        View savingsAccountCard = getLayoutInflater().inflate(R.layout.savings_account_card, null);
//        String lastFourDigitsOfSavingsAccountId = latestSavingsAccount.getAccountId().substring(latestSavingsAccount.getAccountId().length() - 4);
//        ((TextView) savingsAccountCard.findViewById(R.id.savings_account_id_textview)).setText(lastFourDigitsOfSavingsAccountId);
        ((TextView) savingsAccountCard.findViewById(R.id.savings_account_balance_textview))
                .setText(currencyFormatter.format(user.getSavingsBalance()));

        View accountSpacer = new View(this);
        accountSpacer.setMinimumWidth(1);
        accountSpacer.setMinimumHeight(10);

        cardsContainer.addView(checkingsAccountCard);
        cardsContainer.addView(accountSpacer);
        cardsContainer.addView(savingsAccountCard);

        LinearLayout boxesContainer = (LinearLayout) findViewById(R.id.boxes_container);

        View unusedSavingsCard = getLayoutInflater().inflate(R.layout.unused_savings_card, null);
        ((TextView) unusedSavingsCard.findViewById(R.id.unused_savings_amount)).setText(currencyFormatter.format(new Bank().getUnusedSavings()));
        boxesContainer.addView(unusedSavingsCard);
        View unusedSpacer = new View(this);
        unusedSpacer.setMinimumWidth(1);
        unusedSpacer.setMinimumHeight(10);
        boxesContainer.addView(unusedSpacer);

        for (final Box box : boxes) {
            View boxSummaryCard = getLayoutInflater().inflate(R.layout.box_summary_card, null);
            String boxTitle = box.getTitle();
            String boxBalance = currencyFormatter.format(box.getBalance());
            String boxGoal = " / " + currencyFormatter.format(box.getGoalAmount());

            boxSummaryCard.setBackgroundColor(box.getBalance() >= box.getGoalAmount() ? Color.argb(255, 0, 255, 150) : Color.WHITE);

            ((TextView) boxSummaryCard.findViewById(R.id.box_title)).setText(boxTitle);
            ((TextView) boxSummaryCard.findViewById(R.id.box_balance)).setText(boxBalance);
            ((TextView) boxSummaryCard.findViewById(R.id.box_goal)).setText(boxGoal);

            View boxSpacer = new View(this);
            boxSpacer.setMinimumWidth(1);
            boxSpacer.setMinimumHeight(10);

            boxesContainer.addView(boxSummaryCard);
            boxesContainer.addView(boxSpacer);

            boxSummaryCard.findViewById(R.id.box_summary_add_funds).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent addFundsToBoxIntent = new Intent(context, AddFundsToBoxActivity.class);
                    addFundsToBoxIntent.putExtra("accountId", box.getAccountID());
                    addFundsToBoxIntent.putExtra("boxId", box.getBoxID());
                    addFundsToBoxIntent.putExtra("boxTitle", box.getTitle());
                    addFundsToBoxIntent.putExtra("boxBalance", box.getBalance() + "");
                    addFundsToBoxIntent.putExtra("boxGoal", box.getGoalAmount() + "");
                    addFundsToBoxIntent.putExtra("availableSavings", (new Bank()).getUnusedSavings() + "");

                    startActivity(addFundsToBoxIntent);
                }
            });
        }
        boxesContainer.addView(getLayoutInflater().inflate(R.layout.create_new_box_card, null));
    }

    public class Bank {

        Context context;

        public Bank() {
            this.context = context;
        }

        public void updateAccountsByCustomerId(String customerId) {
            boxes = dbTool.getAllBoxesOfAccount(user.getSavingsAccountID());
            placeViews();
//            client.ACCOUNT.getCustomerAccounts(customerId, new NessieResultsListener() {
//
//                @Override
//                public void onSuccess(Object o) {
//                    boxes = dbTool.getAllBoxesOfAccount(user.getSavingsAccountID());
//
////                    Account[] accounts = (new Bank()).getAccountsFromString(o.toString());
////                    latestCheckingsAccount = accounts[0];
////                    latestSavingsAccount = accounts[1];
//
//                    placeViews();
//                }
//
//                @Override
//                public void onFailure(NessieError nessieError) {
//                    System.out.println("FAIL FAIL FAIL FAIL FAIL");
//                    System.out.println(nessieError.getMessage());
//                }
//            });
        }

        public Account[] getAccountsFromString(String accountString) {
            accountString = accountString.replace("[", "");
            accountString = accountString.replace("]", "");
            System.out.println(accountString);

            String[] accountsStrings = accountString.split(Pattern.quote("}, "));
            accountsStrings[0].replace("Account{", "");
            accountsStrings[1].replace("Account{", "");
            accountsStrings[1].replace("}", "");

            SavingsAccount savingsAccount = null;
            CheckingsAccount checkingsAccount = null;

            for (String account : accountsStrings) {
                String type = account.substring(account.indexOf("Type") + 6, account.indexOf(',', account.indexOf("Type") + 6) - 1);
                String balance = account.substring(account.indexOf("Balance") + 8, account.indexOf(',', account.indexOf("Balance") + 8));
                String accountId = account.substring(account.indexOf('=') + 2, account.indexOf(',', account.indexOf('=' + 2)));
                String customerId = account.substring(account.lastIndexOf("=") + 2);
                int b = Integer.parseInt(balance);
                if (type.equals("Savings")){
                    savingsAccount = new SavingsAccount(b, accountId,  customerId);
                } else {
                    checkingsAccount = new CheckingsAccount(b, accountId, customerId);
                }
            }
            Account[] accounts = { checkingsAccount, savingsAccount };
            return accounts;
        }

        public double getUnusedSavings(){
            ArrayList<Box> boxes;
            boxes = dbTool.getAllBoxesOfAccount(user.getSavingsAccountID());
            System.out.println(boxes.size());
            double sumOfBoxes = 0;
            for (Box box : boxes){
                sumOfBoxes += box.getBalance();
            }
            return user.getSavingsBalance() - sumOfBoxes;
        }
    }

}
