package com.pluto.pluto;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.reimaginebanking.api.nessieandroidsdk.NessieError;
import com.reimaginebanking.api.nessieandroidsdk.NessieResultsListener;
import com.reimaginebanking.api.nessieandroidsdk.constants.TransactionMedium;
import com.reimaginebanking.api.nessieandroidsdk.models.Transfer;
import com.reimaginebanking.api.nessieandroidsdk.requestclients.NessieClient;

import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 * Developer: Sam Javed<br/>
 * Project: pluto<br/>
 * Date: 1/22/2017<br/>
 * Time: 3:47 AM<br/>
 */

public class TransferCheckingsToSavingsActivity extends Activity {

    String username;
    String checkingsBalance;
    String availableSavings;
    double amountToTransfer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        getWindow().requestFeature(Window.FEATURE_NO_TITLE);

        setContentView(R.layout.transfer_checkings_to_savings);

        getWindow().setLayout((int) (getResources().getDisplayMetrics().widthPixels * 0.90), LinearLayout.LayoutParams.WRAP_CONTENT);

        Intent callingIntent = getIntent();
        username = callingIntent.getStringExtra("username");
        checkingsBalance = callingIntent.getStringExtra("checkingsBalance");

        ((TextView) findViewById(R.id.checking_transfer_current_amount)).setText(MainActivity.currencyFormatter.format(Double.parseDouble(checkingsBalance)));

        findViewById(R.id.transfer_checking_to_savings_submit).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                amountToTransfer = Double.parseDouble(((EditText) findViewById(R.id.amount_to_transfer)).getText().toString());

                if (amountToTransfer <= Double.parseDouble(checkingsBalance)) {
                    transfer();
                    finish();
                }
            }

            public void transfer() {
                User user = MainActivity.dbTool.getUser(username);
                transferFromCheckingToSaving(amountToTransfer, user.getCheckingsAccountID(), user.getSavingsAccountID());
            }
        });
    }

    public void transferFromCheckingToSaving(double amount, String checkingAccountId, String savingsAccountId){
        NessieClient client = NessieClient.getInstance("9a233163a933edb4616ff218a63db9d8");

        Calendar calendar = GregorianCalendar.getInstance();
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        int month = calendar.get(Calendar.MONTH) + 1;
        int year = calendar.get(Calendar.YEAR);

        final Transfer transfer = new Transfer.Builder().amount(amount).medium(TransactionMedium.BALANCE).transactionDate(year + "-" + month + "-" + day).payeeId(savingsAccountId).build();

        client.TRANSFER.createTransfer(checkingAccountId, transfer, new NessieResultsListener() {
            @Override
            public void onSuccess(Object result) {
                User user = MainActivity.dbTool.getUser(username);
                user.setSavingsBalance(user.getSavingsBalance() + transfer.getAmount());
                user.setCheckingsBalance(user.getCheckingsBalance() - transfer.getAmount());

                MainActivity.dbTool.saveUser(user);
            }

            @Override
            public void onFailure(NessieError error) {
                System.out.println(error.getMessage());
            }
        });
    }

}
