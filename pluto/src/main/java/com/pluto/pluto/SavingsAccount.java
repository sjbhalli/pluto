package com.pluto.pluto;

/**
 * Developer: Sam Javed<br/>
 * Project: Pluto<br/>
 * Date: 1/21/2017<br/>
 * Time: 3:10 PM<br/>
 */

public class SavingsAccount implements Account {

    String nickname;
    int balance;
    String accountId;
    String accountNumber;
    String customerId;
    Box[] boxes;

    public SavingsAccount(int balance, String accountId, String customerId) {
        //this.nickname = nickname;
        this.balance = balance;
        this.accountId = accountId;
//        this.accountNumber = accountNumber;
        this.customerId = customerId;
//        this.boxes = boxes;
    }

    @Override
    public String getNickname() {
        return nickname;
    }

    @Override
    public double getBalance() {
        return balance;
    }

    @Override
    public String getAccountId() {
        return accountId;
    }

    @Override
    public String getAccountNumber() {
        return accountNumber;
    }

    @Override
    public String getCustomerId() {
        return customerId;
    }

    public Box[] getBoxes() {
        return boxes;
    }
}
