package com.pluto.pluto;

/**
 * Developer: Sam Javed<br/>
 * Project: Pluto<br/>
 * Date: 1/21/2017<br/>
 * Time: 3:10 PM<br/>
 */

public interface Account {

    public String getNickname();
    public double getBalance();
    public String getAccountId();
    public String getAccountNumber();
    public String getCustomerId();

}
