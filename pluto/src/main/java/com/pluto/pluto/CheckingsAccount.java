package com.pluto.pluto;

/**
 * Developer: Sam Javed<br/>
 * Project: Pluto<br/>
 * Date: 1/21/2017<br/>
 * Time: 3:10 PM<br/>
 */

public class CheckingsAccount implements Account {

    String nickname;
    int balance;
    String accountId;
    String accountNumber;
    String customerId;

    public CheckingsAccount(int balance, String accountId, String customerId) {
        //this.nickname = nickname;
        this.balance = balance;
        this.accountId = accountId;
        //this.accountNumber = accountNumber;
        this.customerId = customerId;
    }

    @Override
    public String getNickname() {
        return nickname;
    }

    @Override
    public double getBalance() {
        return balance;
    }

    @Override
    public String getAccountId() {
        return accountId;
    }

    @Override
    public String getAccountNumber() {
        return accountNumber;
    }

    @Override
    public String getCustomerId() {
        return customerId;
    }
}
