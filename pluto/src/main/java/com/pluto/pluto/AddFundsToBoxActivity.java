package com.pluto.pluto;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.renderscript.Double2;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

/**
 * Developer: Sam Javed<br/>
 * Project: pluto<br/>
 * Date: 1/22/2017<br/>
 * Time: 1:41 AM<br/>
 */

public class AddFundsToBoxActivity extends Activity {

    String accountId;
    String boxId;
    String boxTitle;
    String boxBalance;
    String availableSavings;
    double amountToTransfer = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        getWindow().requestFeature(Window.FEATURE_NO_TITLE);

        setContentView(R.layout.add_funds_to_box);

        getWindow().setLayout((int) (getResources().getDisplayMetrics().widthPixels * 0.90), LinearLayout.LayoutParams.WRAP_CONTENT);

        Intent callingIntent = getIntent();
        accountId = callingIntent.getStringExtra("accountId");
        boxId = callingIntent.getStringExtra("boxId");
        boxTitle = callingIntent.getStringExtra("boxTitle");
        boxBalance = callingIntent.getStringExtra("boxBalance");
        availableSavings = callingIntent.getStringExtra("availableSavings");

        ((TextView) findViewById(R.id.add_funds_to_box_title)).setText(boxTitle);
        ((TextView) findViewById(R.id.add_funds_to_box_current_amount)).setText(MainActivity.currencyFormatter.format(Double.parseDouble(boxBalance)));
        ((TextView) findViewById(R.id.available_savings_textview)).setText(MainActivity.currencyFormatter.format(Double.parseDouble(availableSavings)));

        findViewById(R.id.add_funds_to_box_submit).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                amountToTransfer = Double.parseDouble(((EditText) findViewById(R.id.amount_to_add)).getText().toString());

                if (amountToTransfer <= Double.parseDouble(availableSavings)) {
                    transfer();
                    finish();
                }
            }

            public void transfer() {
                Box box = MainActivity.dbTool.getBoxById(accountId, boxId);
                box.setBalance(box.getBalance() + amountToTransfer);
                MainActivity.dbTool.saveBox(box);
            }
        });
    }
}
